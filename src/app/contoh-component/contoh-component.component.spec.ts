import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContohComponentComponent } from './contoh-component.component';

describe('ContohComponentComponent', () => {
  let component: ContohComponentComponent;
  let fixture: ComponentFixture<ContohComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContohComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContohComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
