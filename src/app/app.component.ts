import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  
  constructor(public translate: TranslateService) {
    translate.addLangs(['en', 'ru']);
      translate.setDefaultLang('en');

      const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|ru/) ? browserLang : 'en')
  }

  title = 'multiapp';
  today = Date.now();
  
  
  users = [
    { name: 'John', gender: 'male' },
    { name: 'Jill', gender: 'female' }
  ]
  usersCount = this.users.length


}

